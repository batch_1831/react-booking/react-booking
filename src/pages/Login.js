import {useEffect, useState, useContext} from 'react';
import UserContext from '../UserContext';
import {Button, Form} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login(){

    // Allows us to consume the User Context object and its properties to use for the user validation
    const {user, setUser} = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false);



    function login(e){

        e.preventDefault();

        // Process a fetch request to the corresponding backend API.
            // Syntax: 
                // fetch("url", {options});

                    // Convert the information retrieve from the backend into a JavaScript object
                // .then(res => res.json())

                    // Capture the convertedd data.
                // .then(data => {})
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                // values are coming from our State hooks
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            console.log(data.access)

            if(typeof data.access !== "undefined"){

            localStorage.setItem("token", data.access);
            retrieveUserDetails (data.access);

            Swal.fire({
                title: 'Login Successful',
                icon: 'success',
                text: 'Welcome to Zuitt!'
                });
            }

            else{
                Swal.fire({
                    title: 'Authentication Failed',
                    icon: 'error',
                    text: 'Check your login details and try again.'
                    });
            }
        })


        // Syntax: localStorage.setItem("propertyName", value);
        // Set the email of the authenticated user in the local storage.
        // localStorage.setItem("email", email);

        // setUser({
        //             // email used upon logging in.
        //     email: localStorage.getItem("email")
        // })

        setEmail("");
        setPassword("");
        // alert(`${email} has been verified! Welcome back!`)
    };

    // Retrieve user details 
    // we will get the payload from the token
    const retrieveUserDetails = (token) => {
        // The token will be sent as part of the request 

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`

            }
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
        
    }


    useEffect(() => {
        if(email !== "" && password !== ""){
            setIsActive(true);
        }

        else{
            setIsActive(false);
        }
    }, [email, password])


    return(

        (user.id !== null)
        ?
            // Redirected to the /courses endpoint
            <Navigate to="/courses" />
        :
        <>
            <h1 className="my-5 text-center">Login</h1>
            <Form onSubmit = {(e) => login(e)} className="mb-5">
                <Form.Group className="mb-3" controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} />
                    <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} />
                </Form.Group>

                {
                    isActive
                    ?
                    <Button variant="success" type="submit" id="loginBtn">
                    Submit
                    </Button>
                    :
                    <Button variant="success" type="submit" id="loginBtn" disabled>
                    Submit
                    </Button>
                }
            </Form>
        </>
    )
};
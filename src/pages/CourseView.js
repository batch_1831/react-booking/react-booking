 import {useState, useEffect, useContext} from 'react';
 import {Link, useParams, useNavigate } from 'react-router-dom';
 import {Container, Card, Button, Row, Col} from 'react-bootstrap';
 import UserContext from '../UserContext';

 import Swal from 'sweetalert2';
 
 export default function CourseView() {

    // To check if their is already a logged in user. Change the button from "Enroll" to "Login" if the user is not logged in
    const {user} = useContext(UserContext);

   //Allow us to gain access to methods that will redirect a user to different page after enrolling a course.
    const navigate = useNavigate();

    // Use params hook allows us to retrieve the courseId passed via URL.
    const {courseId} = useParams();


    // Create state hooks to capture the information of a specific course and display it in our application

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const[price, setPrice] = useState(0);

    const enroll = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                courseId: courseId
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data === true){
                Swal.fire({
                    title: 'Successfully enrolled!',
                    icon: 'success',
                    text: 'You have successfully enrolled for this course.'
                })
                navigate('/courses');
            }
            else{
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please try again.'
                    })
                }
            })
        }


    useEffect(() => {
        // console.log(courseId);
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })


    }, [courseId])

    return(
        <Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8 am - 5 pm</Card.Text>
							<div className='d-grid gap-2'>

                            {
                                (user.id !== null)
                                ?
                                    <Button variant="primary"  size="lg" onClick={() => enroll(courseId)}>Enroll</Button>
                                :
                                    <Button as={Link} to='/login' variant="primary"  size="lg">Login in to Enroll</Button>

                            }
                            
                            </div>

						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
    )
};
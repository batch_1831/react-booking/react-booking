// import coursesData from "../data/coursesData";
import CourseCard from '../components/CourseCard';
import { useEffect, useState } from 'react';
export default function Courses(){

    // State that will be use to store the courses retrieved from the database
    const [courses, setCourses] = useState([]);

    // Retrieve the courses from the database upon initial render of the Course component

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses`)
    
    .then(res => res.json())
    .then(data => {
        console.log(data);

        setCourses(data.map(course => {
            return (
                <CourseCard key={course._id} courseProp={course} />
                )
            }))
        })
    }, [])

    // Check if we can access the mock database
    // console.log(coursesData);
    // console.log(coursesData[0]);

    // Props
        // is a shorthand for property since components are considered object in ReactJS
        // Prop is a way to pass data from the parent to child component.
        // it is synonymous to the function parameter

        //  map method
        // const courses = coursesData.map(course => {
        //     return(
        //         <CourseCard key={course.id} courseProp={course} />
        //     )
        // });
    
    return(
        <>
            <h1>Courses</h1>
            {courses}
        </>
    );
}


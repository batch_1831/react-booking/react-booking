import {useEffect, useState} from "react";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import { UserProvider } from "./UserContext";

import AppNavbar from './components/AppNavbar';

import Courses from './pages/Courses';
import CourseView from './pages/CourseView';

import Error from "./pages/Error";
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from "./pages/Logout";
import Register from './pages/Register';


import './App.css';
import {Container} from "react-bootstrap";


function App() {

  // Global State
  // To store the user information and will be used for validating if a user is already logged in on the app or not.
  const [user, setUser] = useState({
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  });

  // wew can check the changes in our User state.
  console.log(user);

  // function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  };

  useEffect(() => {
    // console.log(user);
    // console.log(localStorage);
  }, [user])

  // to update the User State upon page load if a user already exist
  useEffect( () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`

        }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        if(typeof data._id !== "undefined" && typeof data.isAdmin){
          setUser({

            // undefined
            id: data._id,
            isAdmin: data.isAdmin
          })
        }

        // set back the initial state of the user
        else{
          setUser({

            // undefined
            id: null,
            isAdmin: null
          })
        }
        
    })
  }, [])

  return (

    // In Reactjs, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component

    // All other components/pages will be contained in our main component: <App />

    // ReactJS does not like rendering two adjacent elements. instead the adjacent element must be wrapped by a parent element/react fragment

    // ReactJS is a single page application (SPA)
    
    // We store user information in the context by providing the information using the corresponding "Provider" compoent and passing the information via the "value" prop/attribute
    <UserProvider value={{user, setUser, unsetUser}}>

      {/* Router component is used to wrapped around all components which will have access to our routing system. */}
      <Router> 
      <AppNavbar />
      <Container fluid>
        {/* Routes holds all our Route components */}
        <Routes>
        {/* 
            - Route assigns an endpoint and displays the appropriate page component for that endpoint

            - "path" attribute to assign the endpoint

            - "element" attribute assigns page component to be displayed at the endpoint


         */}
          <Route exact path = "/" element={<Home />} />
          <Route exact path = "/courses" element={<Courses />} />
          <Route exact path = "/courses/:courseId" element={<CourseView />} />
          <Route exact path = "/register" element={<Register />} />
          <Route exact path = "/login" element={<Login />} />
          <Route exact path = "/logout" element={<Logout />} />
          <Route exact path = "*" element={<Error />} />



        </Routes>
      </Container>
    </Router>
    </UserProvider>
    //fragment (<>, </>)
  );
}

export default App;

/*
    Mini Activity

    1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
        - The course name should be in the card title.
        - The description and price should be in the card subtitle.
        - The value for description and price should be in the card title.
        - Add a button for Enroll.
    2. Render the CourseCard component in the Home page.
    3. Take a screenshot of your browser and send it in the batch hangouts

*/
import{useState, useEffect} from "react";
import {Card, Button} from "react-bootstrap";
import { Link } from "react-router-dom";

// Decontruct the "courseProp" from the "props" object to shoerten syntax
export default function CourseCard({courseProp}){
    //check to see if the data passed successfully
    // passed props is commonly is an object within an object
    // console.log(props.courseProp.name);

    // Deconstruct courseProp properties into their own variable
    const {_id, name, description, price, slots} = courseProp;

    // States
        // States are used to keep track the information relate to individual components.
    
    // Hooks
        // Special react defined method and functions that allow us to do certain tasks in our components.
        // Use the state hook for this component to be able to store its state.

    // Syntax: 
        // const [stateName, setStateName] = useState(initialStateValue);

    // useState() is a hook that creates states.
        // useState() returns an array with 2 items
            // The first item in the is the state
            // and the second one is the setter function

        // Array destructuring for the useState()
        // const [count, setCount] = useState(0);
        // console.log(useState(0));

/* 
    function enroll () {
        if(count < 10){
            setCount(count + 1);
        }
        else{
            alert("No more seats")
        }
        console.log("Enrolees :" + count);
    };
*/

    // Activity solution
    // const [seats, setSeats] = useState(10);
    // const [isOpen, setIsOpen] = useState(false);

    // function enroll(){

    //     setCount(count + 1);
    //     // console.log("Enrolees: " + count);

    //     setSeats(seats - 1);
    //     // console.log("Seats: " + seats);

    // };

    // function unEnroll() {
    //     setCount(count - 1);
    // };

    // useEffect() 
        // - allows us to run a task or an effect, the difference is that with useEffect we can manipulate when it will run.
            // Syntax:
                // useEffect(function, [dependency]);

    // useEffect(() => {
    //     if(seats === 0){
    //         alert("No more seats available");
    //         setIsOpen(true);
    //     }
    // }, [seats]);


    // If the useEffect() does not have a dependency array, it will run on the initial render and whenever a state is set by its set function
    // useEffect(() => {
    //     // Runs on every render
    //     console.log("useEffect render");
    // });


    // If the useEffect() has a dependency array but empty it will only run on initial render.
    // useEffect(() => {
    //     // run only on the initial render
    //     console.log("useEffect Render");
    // }, []);


    // if the useEffect() has a dependency array and there is state or data in it, the useEffect() wil run whenever that state is updated.
    // useEffect(() => {
    //     // Runs on a inital render
    //     // and everytime the dependency value will change (seats)
    //     console.log("useEffect Render");
    // }, [seats]);


    return(
        <Card className="p-3 my-3">
            <Card.Body>
                <Card.Title>
                    {name}
                </Card.Title>

                <Card.Subtitle>
                    Description:
                </Card.Subtitle>

                <Card.Text>
                    {description}
                </Card.Text>

                <Card.Subtitle>
                    Price:
                </Card.Subtitle>

                <Card.Text>
                    {price}
                </Card.Text>

                <Card.Text>
                    Slots: {slots}
                </Card.Text>
                <Button  as={Link} to={`/courses/${_id}`} variant="primary">Details</Button>

                {/* <Button variant="primary mx-1" onClick={enroll}>Enroll</Button>
                <Button variant="danger mx-1" onClick={unEnroll}>Unenroll</Button> */}
            </Card.Body>
    </Card>
  );
}
// import Navbar from "react-bootstrap/Navbar";
// import Nav from "react-bootstrap/Nav";

import {Link} from 'react-router-dom';

import { useState, useContext } from 'react';
import Usercontext from '../UserContext';

import {Navbar, Nav, Container} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function AppNavbar(){

    const {user} = useContext(UserContext);

    // state hook to store the information stored in the login page
                                        // null
    // const [user, setUser] = useState(localStorage.getItem("email"));
    // console.log(user);

    return(
        <Navbar bg="dark" variant='dark' expand="lg">
        <Container>
            <Navbar.Brand as={Link} to="/" >Zuitt</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            {/*className is use instead of "class", to select a CSS classes */}
            <Nav className="ms-auto" defaultActiveKey="/">
                <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>

                <Nav.Link as={Link} to="/courses" eventKey="/courses">Course</Nav.Link>

                {
                    (user.id !== null)
                    ?
                        <Nav.Link as={Link} to="/logout" eventKey="/login">Logout</Nav.Link>

                    :
                        <>
                            <Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>

                            <Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
                        </>

                }
            </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
    )
};
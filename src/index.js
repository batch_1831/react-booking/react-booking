import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// Import the bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



/*
  The syntax used in React.js is JSX
  JSX - JavaScript + XML
    - we are able to create HTML elements using JS.

*/

// const name = "John Smith";

// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }

// function formatname(user){
//   return user.firstName + " " + user.lastName;
// };

// const element = <h1>Hello , {formatname(user)}</h1>;

// // root.render() - allows us to render/display our React.js elements in our HTML file.
// root.render(element);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

